package tw.ghoti.td.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import tw.ghoti.td.TDGame
import tw.ghoti.td.tools.Assets

class SplashScreen(private val game: TDGame) : Screen {
    private val texture = Texture(Gdx.files.internal("badlogic.jpg"))
    private val splashImg = Image(texture)
    private val stage = Stage()

    override fun show() {

        stage.addActor(splashImg)

        /* placed in the center of the screen */
        splashImg.setPosition(Gdx.graphics.width / 2 - splashImg.width / 2, Gdx.graphics.height / 2 - splashImg.height / 2)

        /* assets */
        Assets.load()

        /* animate */
        splashImg.addAction(Actions.sequence(
                Actions.alpha(0f),
                Actions.fadeIn(1f),
                Actions.delay(1f),
                Actions.run {
                    while (!Assets.manager.update()) {
                        Gdx.app.log("assets loading.. ", "${Assets.manager.progress * 100}%")
                    }
                    Gdx.app.log("assets loading.. ", "$100%")
                    game.screen = GameScreen(game)
                }
        ))
    }

    override fun render(delta: Float) {
        /* clear */
        Gdx.gl20.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT)


        stage.act(delta)
        stage.draw()

    }

    override fun resize(width: Int, height: Int) {
        val aspectRatio = height.toFloat() / width.toFloat()
        stage.viewport.update(TDGame.WIDTH, (TDGame.WIDTH.toFloat() * aspectRatio).toInt(), true)
    }

    override fun pause() {}

    override fun resume() {}

    override fun hide() {
        dispose()
    }

    override fun dispose() {
        texture.dispose()
        stage.dispose()
    }
}
