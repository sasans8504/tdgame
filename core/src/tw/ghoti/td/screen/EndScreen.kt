package tw.ghoti.td.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import tw.ghoti.td.TDGame
import tw.ghoti.td.TDGame.Companion.HEIGHT
import tw.ghoti.td.TDGame.Companion.WIDTH

class EndScreen(private val game: TDGame): Screen{
    private val camera = OrthographicCamera()
    private val batch = SpriteBatch()
    private val rank = BitmapFont()
    private val font = BitmapFont()

    init {
        camera.setToOrtho(false, WIDTH.toFloat(), HEIGHT.toFloat())
        rank.data.setScale(4f)
        rank.setColor(1f, 0f, 0f, 1f)
        font.setColor(1f, 1f, 1f, 1f)
    }

    override fun hide() {
    }

    override fun show() {
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0.4f, 0.3f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        camera.update()
        batch.projectionMatrix = camera.combined

        batch.begin()
        rank.draw(batch, checkPoint(), WIDTH / 2f - 100f, HEIGHT - 200f)
        font.draw(batch, "round:${game.gameRound}", WIDTH / 2f - 100f, HEIGHT - 280f)
        font.draw(batch, "money:${game.money}", WIDTH / 2f - 100f, HEIGHT - 300f)
        font.draw(batch, "press Enter to retry", WIDTH / 2f - 100f, HEIGHT - 320f)
        batch.end()

        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)){
            game.screen = GameScreen(game)
            dispose()
        }
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
        batch.dispose()
        font.dispose()
        rank.dispose()
    }

    fun checkPoint(): String{
        return when{
            game.gameRound < 10 -> "Rank C"
            game.gameRound in 10..30 -> "Rank B"
            game.gameRound in 30..50 -> "Rank A"
            game.gameRound > 50 -> "Rank S"
            else -> "Rank ???"
        }
    }

}