package tw.ghoti.td.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import tw.ghoti.td.TDGame
import tw.ghoti.td.TDGame.Companion.HEIGHT
import tw.ghoti.td.TDGame.Companion.WIDTH
import tw.ghoti.td.controller.*
import kotlin.math.min

class GameScreen(private val game: TDGame) : Screen {
    companion object {
        /* useful word */
        const val WORD_NONE = "none"
        const val WORD_DELETE = "delete"
        const val WORD_MOVE = "move"
        const val WORD_UPGRADE = "upgrade"
        /* constant */
        const val FONT_SHOWTIME = 3000
        const val ANGLE = 0f
        const val VEL_BULLET = 8f
        const val VEL_ENEMY = 2f
//        const val VEL_ENEMY = 16f
        const val TIME_BULLET_LIFETIME = 1500
        const val TIME_FIRE_PERIOD = 700
        const val BUILD_SPEND = 50
        const val BUILD_WINNIE_SPEND = 317
        const val UPGRADE_SPEND = 45
        const val UPGRADE_WINNIE_SPEND = 131
        const val DELETE_RETURN_SCALE = 0.8
        /* box type */
        const val SENSOR_TEST = "test"
        const val SENSOR_START = "startLine"
        const val SENSOR_END = "endLine"
        const val SENSOR_MENU = "menu"
        const val SENSOR_RANGE = "range"
        const val SENSOR_ROAD = "road"
        const val BODY_SYSTEM = "system"
        const val BODY_TOWER = "tower"
        const val BODY_ENEMY = "enemy"
        const val BODY_BULLET = "bullet"
        /* category */
        const val CAT_SYSTEM: Short = 2
        const val CAT_TOWER: Short = 4
        const val CAT_ENEMY: Short = 8
        const val CAT_BULLET: Short = 16
        const val CAT_RANGE: Short = 32
        /* tower name */
        const val TOWER_NAME_GUN = "gun"
        const val TOWER_NAME_SNI = "snipe"
        const val TOWER_NAME_CAN = "canon"
        const val TOWER_NAME_WIN = "winnie"
        /* tower type */
        const val TOWER_GUN = "guntower"
        const val TOWER_SNI = "snipertower"
        const val TOWER_CAN = "canontower"
        const val TOWER_WIN = "winnietower"
        /* skin name */
        const val SKIN_GUN = "tower_gun_skin"
        const val SKIN_SNI = "tower_snipe_skin"
        const val SKIN_CAN = "tower_canon_skin"
        const val SKIN_WIN = "tower_winnie_skin"

        val moveCoordinate: Map<Int, String> = mapOf(
                0 to "123.0,714.0",
                1 to "123.0,84.0",
                2 to "383.0,84.0",
                3 to "383.0,629.0",
                4 to "1130.0,629.0",
                5 to "1130.0,399.0",
                6 to "650.0,399.0",
                7 to "650.0,124.0",
                8 to "1106.0,124.0"
        )
    }

    private val world: World = World(Vector2(0.0f, 0.0f), false)
    private val b2dr: Box2DDebugRenderer = Box2DDebugRenderer()
    private val camera: OrthographicCamera = OrthographicCamera()
    private var enemyCount = 1
    private var start = false
    private var autoStart = false
    private var startLock = false
    private var enemyClock = System.currentTimeMillis()
    private var startClock = System.currentTimeMillis()
    private var gameMG = GameMG(world)
    private var bodyMG = BodyMG(gameMG, world)
    private var drawMG = DrawMG(gameMG)
    private var stageMG = StageMG(gameMG, world)

    init {
        world.setContactListener(Collision(gameMG))
        camera.setToOrtho(false, WIDTH.toFloat(), HEIGHT.toFloat())
    }

    override fun hide() {

    }

    override fun show() {
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0.4f, 0.3f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()

        drawMG.background(delta)
        stageMG.drawRender()
        drawMG.special(delta)

        gameMG.render()

        testUnit()
//        b2dr.render(world, camera.combined)
        world.step(1 / 60f, 6, 2)
        gameMG.remove()

        if (gameMG.gameOver) {
            autoStart = false
            gameMG.gameOver = !gameMG.gameOver
            game.gameRound = gameMG.gameRound
            game.money = gameMG.money
            game.screen = EndScreen(game)
            dispose()
        }
    }

    override fun pause() {

    }

    override fun resume() {

    }

    override fun resize(width: Int, height: Int) {

    }

    override fun dispose() {
        drawMG.dispose()
        stageMG.dispose()
        gameMG.remove()
        b2dr.dispose()
        world.dispose()
    }

    private fun testUnit() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            Gdx.input.justTouched() && gameMG.heldTower != WORD_NONE && gameMG.mouseLocation().x < (WIDTH - 200)
            println("Gdx.input.justTouched() = ${Gdx.input.justTouched()}")
            println("gameMG.heldTower = ${gameMG.heldTower}")
            println("gameMG.mouseLocation().x < (WIDTH - 200) = ${gameMG.mouseLocation().x < (WIDTH - 200)}")
            println("towerbody userData contains true = ${gameMG.towerBodies[gameMG.heldTower]!!.fixtureList[0].userData.toString().contains("true")}")

        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.E)) {
            println("enemyLife = ${gameMG.enemyLife}")
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.F1)) gameMG.money += 520

        if(Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            autoStart = true
        }
        if(autoStart && !startLock && System.currentTimeMillis() - startClock > 10000){
            start = !start
            gameMG.gameRound += 1
            //40 25 normal hard (or easy)
            gameMG.enemyNextLife += if(gameMG.gameRound % 10 == 0) gameMG.gameRound * 160 else gameMG.gameRound * 30
            startLock = true
            startClock = System.currentTimeMillis()
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && !startLock) {
            start = !start
            gameMG.gameRound += 1
            gameMG.enemyNextLife += if(gameMG.gameRound % 10 == 0) gameMG.gameRound * 160 else gameMG.gameRound * 30
            startLock = true
        }
        if (start) {
            when {
                System.currentTimeMillis() - enemyClock > 500 && enemyCount <= 10 -> {
                    gameMG.enemyBodies["enemy$enemyClock"] = bodyMG.bodyMaker(BODY_ENEMY, "enemy$enemyClock")
                    enemyClock = System.currentTimeMillis()
                    enemyCount += 1
                }
                enemyCount > 10 -> {
                    start = !start
                    enemyCount = 1
                }
            }
        }
        if (gameMG.enemyBodies.isEmpty()) {
            startLock = false
            gameMG.enemyLife = gameMG.enemyNextLife
        }

        if (Gdx.input.justTouched() && gameMG.heldTower != WORD_NONE && gameMG.mouseLocation().x < (WIDTH - 200)) {
            if (gameMG.towerBodies[gameMG.heldTower]!!.fixtureList[0].userData.toString().contains("true")) {
                gameMG.towerBodies[gameMG.heldTower]!!.setTransform(gameMG.mouseLocation(), ANGLE)
                gameMG.towerBodies[gameMG.heldTower]!!.userData = gameMG.towerBodies[gameMG.heldTower]!!.userData.toString().replace("wait", "alive")
                stageMG.controlActor(gameMG.heldTower)
                gameMG.heldTower = WORD_NONE
                gameMG.towerSetContactList.clear()
            }
        }
    }

}
