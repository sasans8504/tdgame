package tw.ghoti.td.tools

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.g2d.Animation


object Assets {
    var manager = AssetManager()
    var skin: Skin? = null

    fun load() {

    }

    fun setSkin() {
        if (skin == null) {
            /* Gdx.files.internal("ui/skin/.json"), manager.get(".pack", TextureAtlas.class) */

        }
    }

    fun update(): Boolean {
        return manager.update()
    }

    fun dispose() {
        manager.dispose()
    }
}
