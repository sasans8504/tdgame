package tw.ghoti.td

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import tw.ghoti.td.screen.GameScreen
import tw.ghoti.td.screen.SplashScreen

class TDGame : Game() {
    companion object {
        const val WIDTH = 1366
        const val HEIGHT = 768
    }

    private var vSyncEnable = true
    val assetManager = AssetManager()
    var gameRound = 0
    var money = 0

    override fun create() {
        Gdx.graphics.setVSync(vSyncEnable)
        Gdx.graphics.setResizable(false)
        Gdx.graphics.setTitle("TowerDefend")
        setScreen(SplashScreen(this))
    }

}
