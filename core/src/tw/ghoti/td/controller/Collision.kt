package tw.ghoti.td.controller

import com.badlogic.gdx.physics.box2d.Contact
import com.badlogic.gdx.physics.box2d.ContactImpulse
import com.badlogic.gdx.physics.box2d.ContactListener
import com.badlogic.gdx.physics.box2d.Manifold
import tw.ghoti.td.screen.GameScreen

class Collision(private val gameMG: GameMG) : ContactListener {

    override fun endContact(contact: Contact?) {
        if (contact != null) {
            val fixtureDataA = contact.fixtureA.userData.toString()
            val fixtureDataB = contact.fixtureB.userData.toString()
            val userDataA = contact.fixtureA.body.userData.toString()
            val userDataB = contact.fixtureB.body.userData.toString()
            if ((fixtureDataA.contains(GameScreen.BODY_SYSTEM) || fixtureDataA.contains(GameScreen.BODY_TOWER))
                    && fixtureDataB.contains(GameScreen.BODY_TOWER)) {
                gameMG.towerSetContactList.remove(fixtureDataA)
            }

            if (userDataA.contains(GameScreen.BODY_ENEMY) && userDataB.contains(GameScreen.SENSOR_RANGE)) {
                gameMG.markableList.remove(userDataB + "_${userDataA.split("_")[0]}")
            }

            contact.fixtureB.body.userData = userDataB.replace("alive", "dead")

        }
    }

    override fun beginContact(contact: Contact?) {
        if (contact != null) {
            val fixtureDataA = contact.fixtureA.userData.toString()
            val fixtureDataB = contact.fixtureB.userData.toString()
            val userDataA = contact.fixtureA.body.userData.toString()
            val userDataB = contact.fixtureB.body.userData.toString()
            if ((fixtureDataA.contains(GameScreen.BODY_SYSTEM) || fixtureDataA.contains(GameScreen.BODY_TOWER))
                    && fixtureDataB.contains(GameScreen.BODY_TOWER)) {
                gameMG.towerSetContactList.add(fixtureDataA)
            }

            if (userDataA.contains(GameScreen.BODY_ENEMY) && userDataB.contains(GameScreen.SENSOR_RANGE)) {
                gameMG.markableList.add(userDataB + "_${userDataA.split("_")[0]}")
            }

            if (userDataA.contains(GameScreen.BODY_ENEMY) && userDataB.contains(GameScreen.BODY_BULLET)) {
                damage(contact, userDataA)
                if(!gameMG.getUserdataValue(contact.fixtureB.body.userData, "bulletType").contains(GameScreen.TOWER_NAME_CAN))
                    contact.fixtureB.body.userData = userDataB.replace("alive", "dead")
            }

        }

    }

    override fun preSolve(contact: Contact?, oldManifold: Manifold?) {

    }

    override fun postSolve(contact: Contact?, impulse: ContactImpulse?) {

    }

    fun damage(contact: Contact, enemyData: String) {
        val bulletAttack = gameMG.getUserdataValue(contact.fixtureB.body.userData, "bulletAttack").toInt()
        val life = enemyData.split("life")[1].toInt()
        val newLife = life - bulletAttack
        if (newLife <= 0)
            contact.fixtureA.body.userData = enemyData.replace("alive", "hitdead")
        else
            contact.fixtureA.body.userData = enemyData.replace("life$life", "life$newLife")
    }
}
