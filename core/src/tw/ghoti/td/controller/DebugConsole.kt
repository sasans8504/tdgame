package tw.ghoti.td.controller

import javax.swing.*
import java.awt.*
import java.io.OutputStream
import java.io.PrintStream

class DebugConsole : JFrame {
    internal lateinit var textArea: JTextArea

    constructor() : super() {
        setSize(600, 600)
        isResizable = true
        defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        setLocation(100, 150)
        runDebug()
    }

    private fun runDebug() {
        textArea = JTextArea(24, 80)
        textArea.background = Color.BLACK
        textArea.foreground = Color.LIGHT_GRAY
        textArea.font = Font(Font.MONOSPACED, Font.PLAIN, 12)
        val scrollPane = JScrollPane()
        scrollPane.setViewportView(textArea)
        this.add(scrollPane)
        this.pack()
        this.isVisible = true

        redirectOut()

        println("debug system start")
        println("- function key -")
        println("D - check if tower can't set")
        println("E - check enemy's information")
        println("A - auto start new round")
        println("SPACE - manual start new round")
    }

    private fun redirectOut(): PrintStream {
        val out = object : OutputStream() {
            override fun write(b: Int) {
                textArea.append(b.toChar().toString())
            }
        }
        val ps = PrintStream(out)

        System.setOut(ps)
        System.setErr(ps)

        return ps
    }

}
