package tw.ghoti.td.controller

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import tw.ghoti.td.TDGame.Companion.HEIGHT
import tw.ghoti.td.TDGame.Companion.WIDTH
import tw.ghoti.td.screen.GameScreen
import tw.ghoti.td.screen.GameScreen.Companion.ANGLE
import tw.ghoti.td.screen.GameScreen.Companion.WORD_DELETE
import tw.ghoti.td.screen.GameScreen.Companion.WORD_MOVE
import tw.ghoti.td.screen.GameScreen.Companion.WORD_NONE
import tw.ghoti.td.screen.GameScreen.Companion.WORD_UPGRADE
import kotlin.math.abs

class GameMG(private val world: World) {
    companion object {
        const val GUN_ATTACK = 35
        const val GUN_ATTACK_UP_RATE = 30
        const val GUN_RANGE = 200f
        const val GUN_RANGE_UP_RATE = 25f
        const val GUN_BULLET_RADIUS = 5f
        const val GUN_BULLET_DIAMETER = 2 * GUN_BULLET_RADIUS
        const val SNI_ATTACK = 80
        const val SNI_ATTACK_UP_RATE = 45
        const val SNI_RANGE = 250f
        const val SNI_RANGE_UP_RATE = 35f
        const val SNI_BULLET_RADIUS = 3f
        const val SNI_BULLET_DIAMETER = 2 * SNI_BULLET_RADIUS
        const val CAN_ATTACK = 55
        const val CAN_ATTACK_UP_RATE = 45
        const val CAN_RANGE = 210f
        const val CAN_RANGE_UP_RATE = 50f
        const val CAN_BULLET_RADIUS = 30f
        const val CAN_BULLET_DIAMETER = 2 * CAN_BULLET_RADIUS
        const val WIN_ATTACK = 618
        const val WIN_ATTACK_UP_RATE = 131
        const val WIN_RANGE = 317f
        const val WIN_RANGE_UP_RATE = 131f
        const val WIN_BULLET_RADIUS = 29f
        const val WIN_BULLET_DIAMETER = 2 * WIN_BULLET_RADIUS
    }

    var heldTower = WORD_NONE
    var touchedTower = WORD_NONE
    var gameRound = 0
    var gameLife = 10
    var enemyLife = 70
    var enemyNextLife = enemyLife
    var money = 140
    var killAward = 3
    var bulletName = 0.001
    var towerName = 0
    var informationClock = System.currentTimeMillis()
    var warningClock = System.currentTimeMillis()
    var gameOver = false
    var wrongWord = WORD_NONE

    var sensors: MutableMap<String, Body> = mutableMapOf()
    var enemyBodies: MutableMap<String, Body> = mutableMapOf()
    var bulletBodies: MutableMap<String, Body> = mutableMapOf()
    var towerBodies: MutableMap<String, Body> = mutableMapOf()
    var rangeBodies: MutableMap<String, Body> = mutableMapOf()
    var firingTowers: MutableMap<String, Long> = mutableMapOf()
    var removeList: MutableList<String> = mutableListOf()
    var markableList: MutableList<String> = mutableListOf()
    var towerSetContactList: MutableList<String> = mutableListOf()
    /* tower name , action */
    var stageActiveList: MutableMap<String, String> = mutableMapOf()

    fun render() {
        /* enemy move */
        enemyBodies.forEach { (_, body) ->
            enemyMove(body)
        }

        /* attack */
        for (value in markableList) {
            val names = value.split("_")
            val tower = names[1]
            val target = names[names.size - 1]
            var period = GameScreen.TIME_FIRE_PERIOD
            when {
                tower.contains(GameScreen.TOWER_SNI) -> {
                    period = 1800
                }
                tower.contains(GameScreen.TOWER_CAN) -> {
                    period = 4000
                }
            }
            if (!firingTowers.containsKey(tower) || firingTowers.containsKey(tower) && System.currentTimeMillis() - firingTowers[tower]!! > period) {
                if (!towerBodies[tower]!!.userData.toString().contains("wait")) {
                    bulletFire(tower, towerBodies[tower]!!.position, enemyBodies[target]!!.position, getUserdataValue(towerBodies[tower]!!.userData, "attack"))
                    towerBodies[tower]!!.userData = towerBodies[tower]!!.userData.toString().replace("untarget", target)
                    firingTowers[tower] = System.currentTimeMillis()
                }
            }
        }

        /* bullet move */
        for ((key, value) in bulletBodies) {
            val fireTime = getUserdataValue(value.userData, "lifeTime").toLong()
//            val fireTime = value.userData.toString().split("_")[3].toLong()
            if (System.currentTimeMillis() - fireTime > GameScreen.TIME_BULLET_LIFETIME) {
                removeList.add(key)
            } else {
                bulletMove(key, value)
            }
        }

        /* held tower */
        if (heldTower != WORD_NONE) {
            if (towerBodies.containsKey(heldTower) && mouseLocation().x < WIDTH - 200f) {
                stageActiveList[heldTower] = WORD_MOVE
                towerBodies[heldTower]!!.setTransform(mouseLocation(), ANGLE)
                rangeBodies[heldTower]!!.setTransform(mouseLocation(), ANGLE)
            }else{
                stageActiveList[heldTower] = WORD_MOVE
                towerBodies[heldTower]!!.setTransform(WIDTH - 250f, HEIGHT - 50f, ANGLE)
                rangeBodies[heldTower]!!.setTransform(WIDTH - 250f, HEIGHT - 50f, ANGLE)
            }
        }

        /* game over */
        if (gameLife <= 0) {
            wrongWord = "game over"
            warningClock = System.currentTimeMillis()
            println("game over")
            println("gameRound = $gameRound")
            gameOver = true
        }

        /* check held tower setable */
        if (towerSetContactList.isNotEmpty() && towerBodies.containsKey(heldTower))
            towerBodies[heldTower]!!.fixtureList[0].userData = GameScreen.BODY_TOWER + "_false"

        if (towerSetContactList.isEmpty() && towerBodies.containsKey(heldTower))
            towerBodies[heldTower]!!.fixtureList[0].userData = GameScreen.BODY_TOWER + "_true"

    }

    fun mouseLocation(): Vector2 {
        return Vector2(Gdx.input.x.toFloat(), HEIGHT - Gdx.input.y.toFloat())
    }

    fun getUserdataValue(userData: Any, key: String): String {
        val data = userData.toString().split("_")
        for (value in data) {
            if (value.contains(key)) {
                return value.split(key)[1]
            }
        }
        return "false"
    }

    fun userdataContains(userData: Any, key: String): Boolean {
        val data = userData.toString()
        return data.contains(key)
    }

    fun replaceUserdataValue(body: Body, oldValue: String, newValue: String) {
        body.userData = body.userData.toString().replace(oldValue, newValue)
    }



    private fun enemyMove(body: Body) {
        val enemyInf = body.userData.toString().split("_")
        val name = enemyInf[0]
        val status = enemyInf[1]
        val nowTurn = enemyInf[2].toInt()
        val life = enemyInf[3]
        when (status) {
            "dead" -> removeList.add(name)
            "hitdead" -> {
                money += (killAward + gameRound)
                removeList.add(name)
            }
            else -> {
                val coordinate = GameScreen.moveCoordinate.getValue(nowTurn).split(",")
                var positive = true
                val moveInstanceX = coordinate[0].toFloat() - body.position.x
                val moveInstanceY = coordinate[1].toFloat() - body.position.y
                if (moveInstanceX < 0f || moveInstanceY < 0f) positive = false
                when {
                    abs(moveInstanceX) > 0f -> when {
                        abs(moveInstanceX) < GameScreen.VEL_ENEMY -> body.setTransform(coordinate[0].toFloat(), coordinate[1].toFloat(), 0f)
                        positive -> body.setTransform(body.position.x + GameScreen.VEL_ENEMY, body.position.y, 0f)
                        !positive -> body.setTransform(body.position.x - GameScreen.VEL_ENEMY, body.position.y, 0f)
                    }
                    abs(moveInstanceY) > 0f -> when {
                        abs(moveInstanceY) < GameScreen.VEL_ENEMY -> body.setTransform(coordinate[0].toFloat(), coordinate[1].toFloat(), 0f)
                        positive -> body.setTransform(body.position.x, body.position.y + GameScreen.VEL_ENEMY, 0f)
                        !positive -> body.setTransform(body.position.x, body.position.y - GameScreen.VEL_ENEMY, 0f)
                    }
                    else -> {
                        if (nowTurn < 8) {
                            val newTurn = nowTurn + 1
                            body.userData = "${name}_${status}_${newTurn}_$life"
                        } else {
                            body.userData = name + "_dead_8_$life"
                            gameLife -= 1
                        }
                    }
                }
            }
        }
    }

    private fun bulletFire(name: String, initPosition: Vector2, endPosition: Vector2, attack: String) {
        bulletName += 0.001
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.DynamicBody
        bodyDef.position.set(initPosition)
        val body = world.createBody(bodyDef)
        val shape = CircleShape()
        when {
            name.contains(GameScreen.TOWER_NAME_GUN) -> shape.radius = GUN_BULLET_RADIUS
            name.contains(GameScreen.TOWER_NAME_SNI) -> shape.radius = SNI_BULLET_RADIUS
            name.contains(GameScreen.TOWER_NAME_CAN) -> shape.radius = CAN_BULLET_RADIUS
            name.contains(GameScreen.TOWER_NAME_WIN) -> shape.radius = WIN_BULLET_RADIUS
        }
        val fixtureDef = FixtureDef()
        fixtureDef.shape = shape
        fixtureDef.isSensor = true
        fixtureDef.friction = -10f
        fixtureDef.filter.categoryBits = GameScreen.CAT_BULLET
        fixtureDef.filter.maskBits = GameScreen.CAT_ENEMY
        body.createFixture(fixtureDef).userData = GameScreen.BODY_BULLET
        body.userData = GameScreen.BODY_BULLET + "_alive_bulletType${name}_lifeTime${System.currentTimeMillis()}_bulletAttack$attack"
        val x = endPosition.x - initPosition.x
        val y = endPosition.y - initPosition.y
        val forceVector = Vector2(x, y)
        body.isBullet = true
        body.applyLinearImpulse(forceVector, Vector2(0f, 0f), true)
        bulletBodies["bullet$bulletName"] = body
    }

    private fun bulletMove(name: String, body: Body) {
        if (body.userData.toString().contains("dead")) {
            removeList.add(name)
        } else {
            val towerType = body.userData.toString()
            val direct = body.linearVelocity
            when {
                towerType.contains(GameScreen.TOWER_NAME_GUN) || towerType.contains(GameScreen.TOWER_NAME_WIN) -> {
                    body.setTransform(body.position.x + (direct.x / GameScreen.VEL_BULLET), body.position.y + (direct.y / GameScreen.VEL_BULLET), 0f)
                }
                towerType.contains(GameScreen.TOWER_NAME_SNI) -> {
                    body.setTransform(body.position.x + direct.x, body.position.y + direct.y, 0f)
                }
                towerType.contains(GameScreen.TOWER_NAME_CAN) -> {
                    body.setTransform(body.position.x + (direct.x / GameScreen.VEL_BULLET), body.position.y + (direct.y / GameScreen.VEL_BULLET), 0f)
                }
            }
        }
    }

    /**
     * don't remove range with tower body
     * that makes tower hard to update range
     */
    fun remove(type: String = WORD_NONE) {
        if (removeList.isNotEmpty()) {
            for (name in removeList) {
                if (name == heldTower) {
                    println("release held tower $heldTower")
                    heldTower = WORD_NONE
                    towerSetContactList.clear()
                }
                if (name == touchedTower) {
                    touchedTower = WORD_NONE
                }
                when {
                    type == WORD_UPGRADE -> {
                        world.destroyBody(rangeBodies[name])
                        rangeBodies.remove(name)
                    }
                    name.contains(GameScreen.BODY_TOWER) -> {
                        world.destroyBody(towerBodies[name])
                        world.destroyBody(rangeBodies[name])
                        towerBodies.remove(name)
                        rangeBodies.remove(name)
                        firingTowers.remove(name)
                        stageActiveList[name] = WORD_DELETE
                    }
                    name.contains(GameScreen.BODY_BULLET) -> {
                        world.destroyBody(bulletBodies[name])
                        bulletBodies.remove(name)
                    }
                    else -> {
                        world.destroyBody(enemyBodies[name])
                        enemyBodies.remove(name)
                    }
                }
            }
            removeList.clear()
        }
    }
}