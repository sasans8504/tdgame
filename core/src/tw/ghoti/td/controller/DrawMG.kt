package tw.ghoti.td.controller

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.*
import tw.ghoti.td.TDGame.Companion.HEIGHT
import tw.ghoti.td.TDGame.Companion.WIDTH
import tw.ghoti.td.screen.GameScreen
import tw.ghoti.td.screen.GameScreen.Companion.WORD_NONE
import tw.ghoti.td.screen.GameScreen.Companion.FONT_SHOWTIME
import tw.ghoti.td.tools.GifDecoder

class DrawMG(private val gameMG: GameMG) {
    private var batch = SpriteBatch()
    private var information = BitmapFont()
    private var warning = BitmapFont()
    var stateTime = 0f

    private val bulletSkinFilePaths: Map<String, Texture> = mapOf(
            GameScreen.SKIN_GUN to Texture("td/towers/static_tower.png"),
            GameScreen.SKIN_SNI to Texture("td/towers/snipe_tower.png"),
            GameScreen.SKIN_CAN to Texture("td/towers/canon_tower.png"),
            GameScreen.SKIN_WIN to Texture("td/towers/winnie_tower.png")
    )

    private val gifFilePaths: Map<String, Animation<TextureRegion>> = mapOf(
            GameScreen.BODY_TOWER to GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("td/texture/gif/pooh.gif").read()),
            GameScreen.BODY_BULLET to GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("td/texture/gif/heart.gif").read()),
            GameScreen.BODY_ENEMY to GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("td/texture/gif/enemy_piglet.gif").read())
    )

    private val textureFilePaths: Map<String, Texture> = mapOf(
            GameScreen.SENSOR_ROAD to Texture("td/texture/road.png"),
            GameScreen.SENSOR_MENU to Texture("td/texture/menu.png"),
            "damage" to Texture("td/texture/damage.png"),
            "enemyLife" to Texture("td/texture/life.png"),
            "bullet" to Texture("td/texture/bullet.png"),
            "range" to Texture("td/texture/range.png")
    )

    private val roadDef: Set<String> = setOf(
            "105,102,36,630",
            "105,66,260,36",
            "365,66,36,545",
            "365,611,747,36",
            "1112,417,36,230",
            "668,381,480,36",
            "632,142,36,275",
            "632,106,492,36"
    )

    init {
        information.color = Color(0f, 0f, 1f, 1f)
        warning.color = Color(1f, 0f, 0f, 1f)
    }

    /**
     * drawing background, information and notice
     */
    fun background(delta: Float) {
        stateTime += delta
        batch.begin()
        for(road in roadDef){
            val def = road.split(",")
            batch.draw(textureFilePaths[GameScreen.SENSOR_ROAD], def[0].toFloat(), def[1].toFloat(), def[2].toFloat(), def[3].toFloat())
        }
        batch.draw(textureFilePaths[GameScreen.SENSOR_MENU], WIDTH - 200f, 0f, 200f, HEIGHT.toFloat())
        warning.draw(batch, "press space\nto start", WIDTH - 185f, HEIGHT - 10f)
        information.draw(batch, "Round:${gameMG.gameRound}", WIDTH - 185f, HEIGHT - 50f)
        information.draw(batch, "life:${gameMG.gameLife}", WIDTH - 185f, HEIGHT - 70f)
        information.draw(batch, "money:${gameMG.money}", WIDTH - 185f, HEIGHT - 90f)

        when {
            /* draw touched information and show range */
            gameMG.touchedTower != WORD_NONE && System.currentTimeMillis() - gameMG.informationClock <= FONT_SHOWTIME -> {
                information.draw(batch, "${gameMG.touchedTower}\n" +
                        "level:${gameMG.getUserdataValue(gameMG.towerBodies[gameMG.touchedTower]!!.userData, "lv")}\n" +
                        "range:${gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.touchedTower]!!.userData, "width")}\n" +
                        "attack:${gameMG.getUserdataValue(gameMG.towerBodies[gameMG.touchedTower]!!.userData, "attack")}",
                        WIDTH - 185f, HEIGHT - 130f)
                val sprite = Sprite(textureFilePaths["range"])
                sprite.setAlpha(0.1f)
                sprite.setPosition(gameMG.rangeBodies[gameMG.touchedTower]!!.position.x - gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.touchedTower]!!.userData, "width").toFloat()
                        , gameMG.rangeBodies[gameMG.touchedTower]!!.position.y - gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.touchedTower]!!.userData, "width").toFloat())
                sprite.setSize(gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.touchedTower]!!.userData, "width").toFloat() * 2,
                        gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.touchedTower]!!.userData, "width").toFloat() * 2)
                sprite.draw(batch)
            }
            System.currentTimeMillis() - gameMG.informationClock > FONT_SHOWTIME && !gameMG.touchedTower.contains(WORD_NONE) -> {
                gameMG.touchedTower += WORD_NONE
            }
        }
        /* show moving tower's range */
        if(gameMG.heldTower != WORD_NONE){
            val sprite = Sprite(textureFilePaths["range"])
            sprite.setAlpha(0.1f)
            sprite.setPosition(gameMG.rangeBodies[gameMG.heldTower]!!.position.x - gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.heldTower]!!.userData, "width").toFloat()
                    , gameMG.rangeBodies[gameMG.heldTower]!!.position.y - gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.heldTower]!!.userData, "width").toFloat())
            sprite.setSize(gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.heldTower]!!.userData, "width").toFloat() * 2,
                    gameMG.getUserdataValue(gameMG.rangeBodies[gameMG.heldTower]!!.userData, "width").toFloat() * 2)
            sprite.draw(batch)
        }

        /* draw enemy's life */
        for ((_, enemy) in gameMG.enemyBodies) {
            val currentFrame = gifFilePaths[GameScreen.BODY_ENEMY]?.getKeyFrame(stateTime, true)
            batch.draw(currentFrame, enemy.position.x - BodyMG.ENEMY_RADIUS, enemy.position.y - BodyMG.ENEMY_RADIUS, BodyMG.ENEMY_DIAMETER, BodyMG.ENEMY_DIAMETER)
            val life = enemy.userData.toString().split("life")[1].toInt()
            if (life < gameMG.enemyLife) {
                batch.draw(textureFilePaths["damage"], enemy.position.x - BodyMG.ENEMY_RADIUS, enemy.position.y + 25f, 100f / 4f, 2f)
                batch.draw(textureFilePaths["enemyLife"], enemy.position.x - BodyMG.ENEMY_RADIUS, enemy.position.y + 25f, (life.toFloat() / gameMG.enemyLife) * 25f, 2f)
            }
        }

        /* show notice word */
        if(gameMG.wrongWord != WORD_NONE){
            warning.draw(batch, gameMG.wrongWord, WIDTH - 185f, 30f)
        }
        if(System.currentTimeMillis() - gameMG.warningClock > 2000){
            gameMG.wrongWord = WORD_NONE
        }

        batch.end()
    }

    /**
     * draw skin
     */
    fun special(delta: Float) {
        batch.begin()
        stateTime += delta
        /* only for winnie tower skin */
        if (gameMG.towerBodies.isNotEmpty()) {
            for ((name, tower) in gameMG.towerBodies) {
                if (name.contains(GameScreen.TOWER_NAME_WIN)) {
                    val currentFrame = gifFilePaths[GameScreen.BODY_TOWER]?.getKeyFrame(stateTime, true)
                    batch.draw(currentFrame, tower.position.x - BodyMG.TOWER_DIAMETER, tower.position.y - BodyMG.TOWER_DIAMETER)
                }
            }
        }
        /* bullet skin */
        if (gameMG.bulletBodies.isNotEmpty()) {
            for ((_, bullet) in gameMG.bulletBodies) {
                val type = gameMG.getUserdataValue(bullet.userData, "bulletType")
                if (type.contains(GameScreen.TOWER_NAME_WIN)) {
                    val currentFrame = gifFilePaths[GameScreen.BODY_BULLET]?.getKeyFrame(stateTime, true)
                    batch.draw(currentFrame, bullet.position.x - GameMG.WIN_BULLET_RADIUS, bullet.position.y - GameMG.WIN_BULLET_RADIUS, GameMG.WIN_BULLET_DIAMETER, GameMG.WIN_BULLET_DIAMETER)
                } else {
                    var radius = 1f
                    var diameter = 1f
                    var frame = "bullet"
                    when {
                        type.contains(GameScreen.TOWER_GUN) -> {
                            radius = GameMG.GUN_BULLET_RADIUS
                            diameter = GameMG.GUN_BULLET_DIAMETER
                            frame = GameScreen.SKIN_GUN
                        }
                        type.contains(GameScreen.TOWER_SNI) -> {
                            radius = GameMG.SNI_BULLET_RADIUS
                            diameter = GameMG.SNI_BULLET_DIAMETER
                            frame = GameScreen.SKIN_SNI
                        }
                        type.contains(GameScreen.TOWER_CAN) -> {
                            radius = GameMG.CAN_BULLET_RADIUS
                            diameter = GameMG.CAN_BULLET_DIAMETER
                            frame = GameScreen.SKIN_CAN
                        }
                    }
                    batch.draw(bulletSkinFilePaths[frame], bullet.position.x - radius, bullet.position.y - radius, diameter, diameter)
                }
            }
        }

        batch.end()
    }

    fun dispose() {
        batch.dispose()
        warning.dispose()
        information.dispose()
    }
}