package tw.ghoti.td.controller

import com.badlogic.gdx.physics.box2d.*
import tw.ghoti.td.TDGame.Companion.HEIGHT
import tw.ghoti.td.TDGame.Companion.WIDTH
import tw.ghoti.td.screen.GameScreen
import kotlin.experimental.or
import kotlin.math.abs

class BodyMG(private val gameMG: GameMG, private val world: World) {
    companion object {
        const val ENEMY_RADIUS = 15f
        const val ENEMY_DIAMETER = 30f
        const val TOWER_RADIUS = 25f
        const val TOWER_DIAMETER = 50f
    }
    init {
        gameMG.sensors[GameScreen.SENSOR_MENU] = bodyMaker(GameScreen.SENSOR_MENU)
        gameMG.sensors[GameScreen.SENSOR_START] = bodyMaker(GameScreen.SENSOR_START)
        gameMG.sensors[GameScreen.SENSOR_END] = bodyMaker(GameScreen.SENSOR_END)
        roadMaker()
    }

    /**
     * need constructor for born coordinate
     * make update and born easier
     */
    fun bodyMaker(type: String, name: String = GameScreen.WORD_NONE): Body {
        var x = WIDTH / 2f
        var y = HEIGHT / 2f
        var width = 10f
        var height = 10f
        var userData = GameScreen.WORD_NONE
        var bodyData = GameScreen.WORD_NONE

        val fixtureDef = FixtureDef()
        when (type) {
            GameScreen.SENSOR_TEST -> {
                userData = GameScreen.BODY_SYSTEM + "_${GameScreen.SENSOR_TEST}"
                fixtureDef.filter.categoryBits = GameScreen.CAT_SYSTEM
                fixtureDef.filter.maskBits = GameScreen.CAT_TOWER
            }
            GameScreen.SENSOR_MENU -> {
                x = WIDTH - 100f
                y = HEIGHT / 2f
                width = 100f
                height = HEIGHT / 2f
                userData = GameScreen.BODY_SYSTEM + "_${GameScreen.SENSOR_MENU}"
                fixtureDef.filter.categoryBits = GameScreen.CAT_SYSTEM
                fixtureDef.filter.maskBits = GameScreen.CAT_TOWER
            }
            GameScreen.SENSOR_START -> {
                x = 123.0f
                y = 714.0f
                width = 25f
                height = 25f
                userData = GameScreen.BODY_SYSTEM + "_${GameScreen.SENSOR_START}"
                fixtureDef.filter.categoryBits = GameScreen.CAT_SYSTEM
                fixtureDef.filter.maskBits = GameScreen.CAT_TOWER
            }
            GameScreen.SENSOR_END -> {
                x = 1106.0f
                y = 124.0f
                width = 25f
                height = 25f
                userData = GameScreen.BODY_SYSTEM + "_${GameScreen.SENSOR_END}"
                fixtureDef.filter.categoryBits = GameScreen.CAT_SYSTEM
                fixtureDef.filter.maskBits = GameScreen.CAT_TOWER
            }
            /**
             * only level 1 using mouse location
             * don't use it if possible.
             */
            GameScreen.SENSOR_RANGE -> {
                x = WIDTH - 250f
                y = HEIGHT - 50f
                when {
                    name.contains(GameScreen.TOWER_NAME_GUN) -> width = GameMG.GUN_RANGE
                    name.contains(GameScreen.TOWER_NAME_SNI) -> width = GameMG.SNI_RANGE
                    name.contains(GameScreen.TOWER_NAME_CAN) -> width = GameMG.CAN_RANGE
                    name.contains(GameScreen.TOWER_NAME_WIN) -> width = GameMG.WIN_RANGE
                }
                bodyData = GameScreen.SENSOR_RANGE + "_${name}_width$width"
                fixtureDef.filter.categoryBits = GameScreen.CAT_RANGE
                fixtureDef.filter.maskBits = GameScreen.CAT_ENEMY
            }
            /**
             * enemy's bodyData: enemyCount + alive + road number + life point
             * alive: alive, dead
             */
            GameScreen.BODY_ENEMY -> {
                x = 123.0f
                y = 714.0f
                width = ENEMY_RADIUS
                height = ENEMY_RADIUS
                userData = GameScreen.BODY_ENEMY + "_$name"
                bodyData = name + "_alive_1_life${gameMG.enemyLife}"
                fixtureDef.filter.categoryBits = GameScreen.CAT_ENEMY
                fixtureDef.filter.maskBits = GameScreen.CAT_RANGE.or(GameScreen.CAT_BULLET)
            }
            /**
             * tower's bodyData: enemyCount + alive + round + target
             * alive: wait, alive
             * body don't minus constant number
             */
            GameScreen.BODY_TOWER -> {
                var attack = 1
                x = WIDTH - 250f
                y = HEIGHT - 50f
                width = TOWER_RADIUS
                height = TOWER_RADIUS
                userData = GameScreen.BODY_TOWER + "_false"
                when {
                    name.contains(GameScreen.TOWER_NAME_GUN) -> attack = GameMG.GUN_ATTACK
                    name.contains(GameScreen.TOWER_NAME_SNI) -> attack = GameMG.SNI_ATTACK
                    name.contains(GameScreen.TOWER_NAME_CAN) -> attack = GameMG.CAN_ATTACK
                    name.contains(GameScreen.TOWER_NAME_WIN) -> attack = GameMG.WIN_ATTACK
                }
                bodyData = name + "_wait_lv1_untarget_attack$attack"
                fixtureDef.filter.categoryBits = GameScreen.CAT_TOWER
                fixtureDef.filter.maskBits = GameScreen.CAT_SYSTEM.or(GameScreen.CAT_TOWER)
            }
            GameScreen.WORD_UPGRADE -> {
                val beforeBody = gameMG.rangeBodies[name]!!
                x = beforeBody.position.x
                y = beforeBody.position.y
                width = gameMG.getUserdataValue(beforeBody.userData, "width").toFloat()
                when {
                    name.contains(GameScreen.TOWER_NAME_GUN) -> width += GameMG.GUN_RANGE_UP_RATE
                    name.contains(GameScreen.TOWER_NAME_SNI) -> width += GameMG.SNI_RANGE_UP_RATE
                    name.contains(GameScreen.TOWER_NAME_CAN) -> width += GameMG.CAN_RANGE_UP_RATE
                    name.contains(GameScreen.TOWER_NAME_WIN) -> width += GameMG.WIN_RANGE_UP_RATE
                }
                bodyData = GameScreen.SENSOR_RANGE + "_${name}_width$width"
                fixtureDef.filter.categoryBits = GameScreen.CAT_RANGE
                fixtureDef.filter.maskBits = GameScreen.CAT_ENEMY
                gameMG.removeList.add(name)
                gameMG.remove(GameScreen.WORD_UPGRADE)
            }
        }

        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.DynamicBody
        bodyDef.position.set(x, y)
        val body = world.createBody(bodyDef)
        if (type == GameScreen.SENSOR_RANGE || type == GameScreen.WORD_UPGRADE || type == GameScreen.BODY_TOWER) {
            val shape = CircleShape()
            shape.radius = width
            fixtureDef.shape = shape
        } else {
            val shape = PolygonShape()
            shape.setAsBox(width, height)
            fixtureDef.shape = shape
        }
        fixtureDef.isSensor = true
        fixtureDef.friction = 0f
        body.createFixture(fixtureDef).userData = userData
        body.userData = bodyData
        return body
    }

    private fun roadMaker() {
        for (i in 1..8) {
            //calculate coordinate and size
            var positive = true
            val x0 = GameScreen.moveCoordinate.getValue(i - 1).split(",")[0].toFloat()
            val y0 = GameScreen.moveCoordinate.getValue(i - 1).split(",")[1].toFloat()
            val x1 = GameScreen.moveCoordinate.getValue(i).split(",")[0].toFloat()
            val y1 = GameScreen.moveCoordinate.getValue(i).split(",")[1].toFloat()
            val xTo1 = x0.toInt() - x1.toInt()
            val yTo1 = y0.toInt() - y1.toInt()
            val width = abs(xTo1 / 2f) + 18f
            val height = abs(yTo1 / 2f) + 18f
            if (xTo1 > 0) positive = false
            if (yTo1 < 0) positive = false
            val x = if (x0 == x1) x0 else ((abs(xTo1) / 2f) + if (positive) x0.toInt() else x1.toInt()) / 1f
            val y = if (y0 == y1) y0 else ((abs(yTo1) / 2f) + if (positive) y1.toInt() else y0.toInt()) / 1f

            //build box
            val bodyDef = BodyDef()
            bodyDef.type = BodyDef.BodyType.StaticBody
            bodyDef.position.set(x, y)
            val body = world.createBody(bodyDef)
            val shape = PolygonShape()
            shape.setAsBox(width, height)
            val fixtureDef = FixtureDef()
            fixtureDef.shape = shape
            fixtureDef.isSensor = true
            fixtureDef.friction = 0f
            fixtureDef.filter.categoryBits = GameScreen.CAT_SYSTEM
            fixtureDef.filter.maskBits = GameScreen.CAT_TOWER
            body.createFixture(fixtureDef).userData = GameScreen.BODY_SYSTEM + "_${GameScreen.SENSOR_ROAD}"
            body.userData = GameScreen.WORD_NONE
        }
    }
}