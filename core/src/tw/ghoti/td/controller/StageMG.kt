package tw.ghoti.td.controller

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import tw.ghoti.td.TDGame.Companion.HEIGHT
import tw.ghoti.td.TDGame.Companion.WIDTH
import tw.ghoti.td.screen.GameScreen
import tw.ghoti.td.screen.GameScreen.Companion.BODY_TOWER
import tw.ghoti.td.screen.GameScreen.Companion.BUILD_SPEND
import tw.ghoti.td.screen.GameScreen.Companion.BUILD_WINNIE_SPEND
import tw.ghoti.td.screen.GameScreen.Companion.DELETE_RETURN_SCALE
import tw.ghoti.td.screen.GameScreen.Companion.SENSOR_RANGE
import tw.ghoti.td.screen.GameScreen.Companion.UPGRADE_SPEND
import tw.ghoti.td.screen.GameScreen.Companion.UPGRADE_WINNIE_SPEND
import tw.ghoti.td.screen.GameScreen.Companion.WORD_DELETE
import tw.ghoti.td.screen.GameScreen.Companion.WORD_MOVE
import tw.ghoti.td.screen.GameScreen.Companion.WORD_NONE
import tw.ghoti.td.screen.GameScreen.Companion.WORD_UPGRADE
import tw.ghoti.td.screen.GameScreen.Companion.FONT_SHOWTIME

class StageMG(val gameMG: GameMG, private val world: World) {
    private val residentButtonFilePaths: Map<String, Texture> = mapOf(
            WORD_DELETE to Texture("td/buttons/resident/delete.png"),
            GameScreen.TOWER_GUN to Texture("td/buttons/resident/gun_tower2.png"),
            GameScreen.TOWER_SNI to Texture("td/buttons/resident/sniper_tower.png"),
            GameScreen.TOWER_CAN to Texture("td/buttons/resident/canon_tower.png"),
            GameScreen.TOWER_WIN to Texture("td/buttons/resident/winnie_tower.png")
    )
    private val temporaryButtonFilePaths: Map<String, Texture> = mapOf(
            WORD_UPGRADE to Texture("td/buttons/temporary/upgrade2.png"),
            "winnie_upgrade" to Texture("td/buttons/temporary/winnie_upgrade.png")
    )
    private val towerFilePaths: Map<String, Texture> = mapOf(
            GameScreen.SKIN_GUN to Texture("td/towers/static_tower.png"),
            GameScreen.SKIN_SNI to Texture("td/towers/snipe_tower.png"),
            GameScreen.SKIN_CAN to Texture("td/towers/canon_tower.png"),
            GameScreen.SKIN_WIN to Texture("td/towers/winnie_tower.png")
    )
    var touchedUpgrade = WORD_NONE
    var upgradeLifeTime = System.currentTimeMillis()
    var stage: Stage = Stage()

    init {
        stage.setDebugInvisible(true)
        Gdx.input.inputProcessor = stage
        buttonMaker()
    }

    fun drawRender() {
        /* held tower skin */
        if (gameMG.heldTower != WORD_NONE && gameMG.stageActiveList.containsKey(gameMG.heldTower)) {
            controlActor(gameMG.heldTower)
        } else if (gameMG.heldTower == WORD_NONE && gameMG.stageActiveList.containsValue(WORD_MOVE)) {
            for ((tower, action) in gameMG.stageActiveList) {
                if (action == WORD_MOVE) gameMG.stageActiveList.remove(tower)
            }
        }

        /* check delete actors */
        if (gameMG.stageActiveList.containsValue(WORD_DELETE)) {
            for ((tower, action) in gameMG.stageActiveList) {
                if (touchedUpgrade.contains(tower))
                    controlActor(touchedUpgrade, action)
            }
        }

        /* keep 1 upgrade arrow */
        if (gameMG.touchedTower != WORD_NONE && gameMG.touchedTower.contains(WORD_NONE)) {
            var name = gameMG.touchedTower.replace(WORD_NONE, "")
            name += WORD_UPGRADE
            if (controlActor(name, "check")) {
                controlActor(name, WORD_DELETE)
                gameMG.touchedTower = WORD_NONE
                touchedUpgrade = WORD_NONE
            }
        }

        if (System.currentTimeMillis() - upgradeLifeTime > FONT_SHOWTIME) {
            controlActor(touchedUpgrade, WORD_DELETE)
        }
        stage.draw()
        stage.act()
        gameMG.stageActiveList.clear()

    }

    fun controlActor(name: String, method: String = WORD_MOVE): Boolean {
        for (actor in stage.actors) {
            if (actor.name == name) {
                when (method) {
                    WORD_MOVE -> {
                        if (actor.x < WIDTH - 200f) {
                            actor.setPosition(gameMG.towerBodies[name]!!.position.x - 25f, gameMG.towerBodies[name]!!.position.y - 25f)
                        }
                    }
                    WORD_DELETE -> {
                        stage.actors.removeValue(actor, true)
                    }
                    "check" -> {
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun buttonMaker() {
        var height = 100
        for ((key, value) in residentButtonFilePaths) {
            val skin = Skin()
            skin.add("up", TextureRegion(value, 0, 0, 100, 100))
            skin.add("down", TextureRegion(value, 0, -2, 100, 100))

            //font is necessary
            val playButton = ImageTextButton("", with(ImageTextButton.ImageTextButtonStyle()) {
                up = skin.getDrawable("up")
                down = skin.getDrawable("down")
                font = BitmapFont()
                font.data.setScale(1f)
                font.color = Color.ORANGE
                this
            })

            playButton.setPosition(WIDTH - 100f, HEIGHT - height.toFloat())
            if (key.contains(WORD_DELETE)) {
                playButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        if (gameMG.heldTower != WORD_NONE) {
                            controlActor(gameMG.heldTower, WORD_DELETE)
                            gameMG.removeList.add(gameMG.heldTower)
                            when {
                                gameMG.heldTower.contains(GameScreen.TOWER_NAME_WIN) -> gameMG.money += BUILD_WINNIE_SPEND
                                else -> gameMG.money += BUILD_SPEND
                            }
                        } else if (gameMG.touchedTower != WORD_NONE) {
                            val name = gameMG.touchedTower
                            gameMG.removeList.add(name)
                            when {
                                gameMG.touchedTower.contains(GameScreen.TOWER_NAME_WIN) -> gameMG.money += (BUILD_WINNIE_SPEND * DELETE_RETURN_SCALE).toInt() * gameMG.getUserdataValue(gameMG.towerBodies[name]!!.userData, "lv").toInt()
                                else -> gameMG.money += (BUILD_SPEND * DELETE_RETURN_SCALE).toInt() * gameMG.getUserdataValue(gameMG.towerBodies[name]!!.userData, "lv").toInt()
                            }
                            controlActor(name, WORD_DELETE)
                        }
                        super.clicked(event, x, y)
                    }
                })
            } else {
                playButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        if (gameMG.heldTower == WORD_NONE) {
                            var canBuild = false
                            when {
                                key.contains(GameScreen.TOWER_NAME_WIN) -> if (gameMG.money >= BUILD_WINNIE_SPEND) canBuild = true
                                else -> if (gameMG.money >= BUILD_SPEND) canBuild = true
                            }

                            if (canBuild) {
                                gameMG.towerName += 1
                                val name: String = key + gameMG.towerName
                                gameMG.towerBodies[name] = BodyMG(gameMG, world).bodyMaker(BODY_TOWER, name)
                                towerSkin(gameMG.towerBodies[name]!!, name)
                                gameMG.rangeBodies[name] = BodyMG(gameMG, world).bodyMaker(SENSOR_RANGE, name)
                                gameMG.heldTower = name
                                when {
                                    key.contains(GameScreen.TOWER_NAME_WIN) -> gameMG.money -= BUILD_WINNIE_SPEND
                                    else -> if (gameMG.money >= BUILD_SPEND) gameMG.money -= BUILD_SPEND
                                }
                            } else {
                                gameMG.wrongWord = "no enough money"
                                gameMG.warningClock = System.currentTimeMillis()
                                println("no enough money")
                            }
                        } else {
                            gameMG.wrongWord = "you held a tower! ${gameMG.heldTower}"
                            gameMG.warningClock = System.currentTimeMillis()
                            println("you held a tower! ${gameMG.heldTower}")
                        }
                        super.clicked(event, x, y)
                    }
                })
            }
            height += 100
            playButton.name = key
            stage.addActor(playButton)
        }
    }

    private fun tempButtonMaker(name: String) {
        val skin = Skin()
        var value = temporaryButtonFilePaths[WORD_UPGRADE]
        if (name.contains(GameScreen.TOWER_NAME_WIN)) value = temporaryButtonFilePaths["winnie_upgrade"]
        val position = gameMG.towerBodies[name]!!.position
        skin.add("up", TextureRegion(value, 0, 0, 70, 40))
        skin.add("down", TextureRegion(value, 0, -2, 70, 40))

        val playButton = ImageTextButton("", with(ImageTextButton.ImageTextButtonStyle()) {
            up = skin.getDrawable("up")
            down = skin.getDrawable("down")
            font = BitmapFont()
            font.data.setScale(1f)
            font.color = Color.ORANGE
            this
        })

        playButton.setPosition(position.x - 30f, position.y + 25f)
        playButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                var canUpgrade = false
                /* keep button alive */
                upgradeLifeTime = System.currentTimeMillis()
                /* keep button unique */
                if (touchedUpgrade != WORD_NONE && !touchedUpgrade.contains(name)) {
                    controlActor(touchedUpgrade, WORD_DELETE)
                    touchedUpgrade = WORD_NONE
                }
                touchedUpgrade = "$name$WORD_UPGRADE"
                when {
                    name.contains(GameScreen.TOWER_NAME_WIN) -> if (gameMG.money >= UPGRADE_WINNIE_SPEND) canUpgrade = true
                    else -> if (gameMG.money >= UPGRADE_SPEND) canUpgrade = true

                }
                if (canUpgrade) {
                    val oldData = gameMG.towerBodies[name]!!.userData
                    val level = gameMG.getUserdataValue(oldData, "lv").toInt()
                    if (level == 5) {
                        gameMG.wrongWord = "top level, can't upgrade."
                        gameMG.warningClock = System.currentTimeMillis()
                        println("top level, can't upgrade.")
                    } else {
                        val attack = gameMG.getUserdataValue(oldData, "attack").toInt()
                        var newAttack = attack
                        when {
                            name.contains(GameScreen.TOWER_NAME_GUN) -> newAttack += GameMG.GUN_ATTACK_UP_RATE * level
                            name.contains(GameScreen.TOWER_NAME_SNI) -> newAttack += GameMG.SNI_ATTACK_UP_RATE * level
                            name.contains(GameScreen.TOWER_NAME_CAN) -> newAttack += GameMG.CAN_ATTACK_UP_RATE * level
                            name.contains(GameScreen.TOWER_NAME_WIN) -> newAttack += GameMG.WIN_ATTACK_UP_RATE * level
                        }
                        gameMG.rangeBodies[name] = BodyMG(gameMG, world).bodyMaker(WORD_UPGRADE, name)
                        gameMG.towerBodies[name]!!.userData = gameMG.towerBodies[name]!!.userData.toString().replace("lv$level", "lv${(level + 1)}")
                        gameMG.towerBodies[name]!!.userData = gameMG.towerBodies[name]!!.userData.toString().replace("attack$attack", "attack$newAttack")
                        gameMG.money -= UPGRADE_SPEND
                        gameMG.touchedTower = name
                        gameMG.informationClock = System.currentTimeMillis()
                    }
                } else {
                    gameMG.wrongWord = "no enough money"
                    gameMG.warningClock = System.currentTimeMillis()
                    println("no enough money")
                }
                super.clicked(event, x, y)
            }
        })

        playButton.name = "$name$WORD_UPGRADE"
        touchedUpgrade = playButton.name
        stage.addActor(playButton)
    }

    /**
     * need tower type to get correct tower
     */
    private fun towerSkin(tower: Body, name: String) {
        val skin = Skin()
        var key = GameScreen.SKIN_GUN
        when {
            name.contains(GameScreen.TOWER_NAME_SNI) -> key = GameScreen.SKIN_SNI
            name.contains(GameScreen.TOWER_NAME_CAN) -> key = GameScreen.SKIN_CAN
            name.contains(GameScreen.TOWER_NAME_WIN) -> key = GameScreen.SKIN_WIN
        }
        val text1 = towerFilePaths[key]
        skin.add("up", TextureRegion(text1, 0, 0, 50, 50))
        skin.add("down", TextureRegion(text1, 0, -1, 50, 50))

        val playButton = ImageTextButton("", with(ImageTextButton.ImageTextButtonStyle()) {
            up = skin.getDrawable("up")
            down = skin.getDrawable("down")
            font = BitmapFont()
            font.data.setScale(1f)
            font.color = Color.ORANGE
            this
        })

        playButton.setPosition(tower.position.x - 25f, tower.position.y - 25f)
        playButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if (touchedUpgrade != WORD_NONE) {
                    controlActor(touchedUpgrade, WORD_DELETE)
                    touchedUpgrade = WORD_NONE
                }
                if (gameMG.getUserdataValue(gameMG.towerBodies[name]!!.userData, "wait") == "false") {
                    gameMG.touchedTower = name
                    gameMG.informationClock = System.currentTimeMillis()
                    upgradeLifeTime = System.currentTimeMillis()
                    tempButtonMaker(name)
                }
                super.clicked(event, x, y)
            }
        })

        playButton.name = name
        stage.addActor(playButton)
    }

    fun dispose() {
        stage.dispose()
    }
}