package tw.ghoti.td.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import tw.ghoti.td.TDGame
import tw.ghoti.td.controller.DebugConsole

object DesktopLauncher {
    @JvmStatic
    fun main(arg: Array<String>) {
        DebugConsole()
        val config = LwjglApplicationConfiguration()
        config.width = TDGame.WIDTH
        config.height = TDGame.HEIGHT
        LwjglApplication(TDGame(), config)
    }
}
